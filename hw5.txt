Q1.
1. (λp.(pz)) (λq.(w (λw.(wqzp))))
2. λp.((pq) λp.(qp))

Q2.
1. λs.s z λq.s q
s is bound in first lambda, z is free in first lambda.
s is free in second lambda, q is bound in second lambda.

2. (λs. s z) λq. w λw. w q z s
s is bound in first lambda, z is free in first lambda.
w is free in second lambda.
w is bound in third lambda, q, z, s are free in third lambda.

3. (λs.s) (λq.qs)
s is bound in first lambda.
q is bound in second lambda, s is free in second lambda.

4. λz. (((λs.sq) (λq.qz)) λz. (z z))
The first s is bound, the first q is free in second lamba.
The second q is bound, the first z is free in third lambda.
The third z and third z are all bound in fourth lambda.

Q3.
1. (λz.z) (λq.q q) (λs.s a)
=>(λq.qq)(λs.sa)
=>(λs.sa)(λs'.s'a')
=>(λs'.s'a')a'
=>aa'

2. (λz.z) (λz.z z) (λz.z q)
=>(λz.zz)(λz.zq)
=>(λz.zq)(λz'.z'q')
=>(λz'.z'q')q'
=>qq'

3. (λs.λq.s q q) (λa.a) b
=>(λq.(λa.a)qq)b
=>(λa.a)bb
=>bb

4. (λs.λq.s q q) (λq.q) q
=>(λs.λq.sqq) (λq'.q') q''
=>(λq.(λq'.q')qq)q''
=>(λq'.q')q''q''
=>q''q''

5. ((λs.s s) (λq.q)) (λq.q)
=>((λs.ss)(λq.q))(λq'.q')
=>(λq.q)(λq''.q'')(λq'.q')
=>(λq''.q'')(λq'.q')
=>(λq'.q')

Q4.
1.
T T => T
T F => T
F T => T
F F => F

2.
T=(λx.λy.x)
F=(λx.λy.y)

T T => T
(λp.λq.p p q) T T
=> T T T
=>(λx.λy.x)(λx.λy.x)(λx.λy.x)
=>(λx.λy.x)
=> T 

T F => T
(λp.λq.p p q) T F
=> T T F
=>(λx.λy.x)(λx.λy.x)(λx.λy.y)
=>(λx.λy.x)
=> T

F T => T
(λp.λq.p p q) F T
=>F F T
=>(λx.λy.y)(λx.λy.y)(λx.λy.x)
=>(λx.λy.x)
=> T

F F => F
(λp.λq.p p q) F F
=> F F F
=>(λx.λy.y)(λx.λy.y)(λx.λy.y)
=>(λx.λy.y)
=> F

Q5.
(λp.pFT)
T=(λx.λy.x)
F=(λx.λy.y)

!T
(λp.pFT)(λx.λy.x)
=> T F T
=>(λx.λy.x)(λx.λy.y)(λx.λy.x)
=>(λx.λy.y)
=> F

!F
(λp.pFT)(λx.λy.y)
=> F F T
=>(λx.λy.y)(λx.λy.y)(λx.λy.x)
=>(λx.λy.x)
=> T

It is similar to an IF statement becauce if the input is T 
then it returns the first parameter, which is F ;
if the input is F then it returns the second parameter T. 